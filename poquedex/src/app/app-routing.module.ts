import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DetailPokemonComponent } from './pages/detail-pokemon/detail-pokemon.component';
import { ListPokemonsComponent } from './pages/list-pokemons/list-pokemons.component';

const routes: Routes = [
  { path: '', component: ListPokemonsComponent},
  { path: 'pokemon/:id', component: DetailPokemonComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
