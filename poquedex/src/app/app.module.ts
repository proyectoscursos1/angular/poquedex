import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PokemonsComponent } from './components/pokemons/pokemons.component';
import { DetailPokemonComponent } from './pages/detail-pokemon/detail-pokemon.component';
import { ListPokemonsComponent } from './pages/list-pokemons/list-pokemons.component';

@NgModule({
  declarations: [
    AppComponent,
    PokemonsComponent,
    DetailPokemonComponent,
    ListPokemonsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule


  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
