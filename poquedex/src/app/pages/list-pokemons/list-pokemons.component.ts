import { PokemonService } from './../../services/pokemon.service';
import { Component, OnInit } from '@angular/core';
import { Pokemon } from 'src/app/interfaces/pokemon.interface';

@Component({
  selector: 'app-list-pokemons',
  templateUrl: './list-pokemons.component.html',
  styleUrls: ['./list-pokemons.component.css']
})
export class ListPokemonsComponent implements OnInit {

  pokemons: Pokemon[] = [];
  offset = 0;

  constructor(private pokemonService : PokemonService) { }

  ngOnInit(): void {
    this.listPokemons();
  }

  listPokemons(){
    this.pokemonService.getPokemons(this.offset).subscribe(res => {
      console.log(res);
      // this.pokemons = [res];
    })
  }

}
