import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Pokemon } from '../interfaces/pokemon.interface';
import { map } from 'rxjs/operators'

@Injectable({
    providedIn: 'root',
})
export class PokemonService {

    API_URL = 'https://pokeapi.co/api/v2/pokemon?';

    constructor(private http: HttpClient) { }

    getPokemons( offset) {
        const params: any = { 
            limit: 898,
            offset,
        };
        console.log(this.API_URL)
        return this.http.get<Pokemon[]>(this.API_URL, { params }).pipe(map((res: any) => res.results));
        
    }

}
